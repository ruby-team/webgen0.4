---- CURRENT ----

* bug in menu style section: should work although page is not in menu, should either be totally independent from Tag/Menu or better integrated
* modify WebSite to allow better use (factor out loading of plugins into own method, do not call init in #render method, ...)
* show error message when configuration file is invalid (Webgen::WebSite)
* add a common class to the menu element (either a or span, depending on Fil/DefaultFileHandler:linkToCurrentPage) for easier referencing in CSS styles (update menu CSS style afterwards for horiz-dd, vert-dd, ...)
* add parameters startLevel, minLevels, maxLevels to horizontal menu style
* make linking to image galleries possible by specifing the gallery file
* send bug report to rdoc maintainer (rdoc does not handle nested method defintions correctly)
* make source installation work again (because of help forum entry)?
* rethink plugin config handling (maybe all in a list which is traversed and stopped when one item returns a value)
* extend usage of tags by allowing the following: {highlight: ruby}content to highlightasd {highlight} and add tag for syntax highlighting???
* add listener which checks that after reading the tree no new nodes are created
* if a meta info is not available on the current page, look for it in the template, then in the template of the template, aso.
  -> not easily or not all possible
* refactor menu style plugins so that indivdual parts of the menu can be overridden (by e.g. singleton methods)
* search for TODO items

---- DOCU ----

* documentation: add a glossary (webgen, node, page file, gallery file, parameter, path pattern, plugin, ...)
* document how to create gallery styles
* documentation: add info about what webgen does during a run (read files, create nodes, write nodes)
* documentation: add a page about YAML and basic use
* document: check for consistent naming (page files, tags,...)
* document homepage structure (where is plugin docu, api docu,...)
* document tag @processOutput changed to @process_output
* param values must not be changed during the run by a plugin!!!, see source code of DefaultTag for an example to override params nonetheless
* document speed boost by precalculating node values (costs memory, saves runtime)
* changed structure of blocks meta_info
* provide a sample blog (with andreas04 style)
* website_template/website_style/gallery_style -> README first infos are shown in help, after --- ignored for infos hash
* files must not have spaces in their names!
* all hooks are now lower case!!!
* DefaultFileHandler#node_for_lang
    - should return the node for the given lang if available
    - default behaviour is to return node itself (useful for pics, stylesheets, ...)
* DirectoryHandler#node_for_lang
    - if no node in lang found, return default node, emit warning
    - if node in lang found, return found node
    - if no index file, return default node, emit warning
* update copyright year in all files
* examples for each type of plugins in extending webgen section -> better into API docs

---- FUTURE ----

* remove duplicate code from file handler plugins' create_node method
* add flag images for language bar which are (control through param) used instead of language abbr
* allow resources to reference URLs???
* allow following configuration file syntax:
  if the key is of the form 'add <name>' then it is assumed that the default value for param name is an array or hash
  and the given entries are added instead of overwriting the array/hash
* google sitemap generator
* blogging and rss handlers
* Configuration:outDirectory should support FTP and SFTP protocols
* tag 'link' which provides a link to the file optionally showing an icon -> see mail
* parameter for including page icons in menus (use key pageIcon in node meta info, if set use it in DefaultMenuStyle#menu_item_details)
* include spell checker (ispell, aspell, MS Word OLE on Win32,...)
* tag 'resume', uses xmlresume2x to provide HTML version of xml resume
* multi-lingual picture/gallery files

---- DONE ----

W check if an xml validator is available in ruby
W think about sequence of actions when program is executed
W think about how the data is stored in tree/which data is stored in tree
W set up directory structure and build system
W write ideas and design considerations
W create sample files to work on
W change exception mechanism to use IDs for exceptions
W update all exceptions to new exception format
W implement hooks mechanism
W enable writing of tree again after hooks mechanism installed
W implement page templates using hooks (after dir read store reference to template file in hash)
W review Node class, remove all plugin dependent things
W review configuration class, remove all plugin dependent things
W check if ansi color output is better (for warnings, debug, etc), should be able to turn off
W restructure current files, make more readable
W pagePlugin.buildNode: srcName = File.basename(srcName) in front of urlName = ...
W pagePlugin.get_template_for_node: raise ... if node.nil?
W change the classes which use the listener/composite module because of the refactorization
W refactor current classes
W implement tag <relocatable> for references that should be relocatable
W FileCopyPlugin: check if file has changed and only then copy
W think about how to print out warnings uniformly --> log4r
W define format of config.yaml
W redo configuration file as yaml file
W meta tag replacer - replaces tag with contents of equally named node.metainfo entry
W add yaml page files
W add default values for configuration in configuration.rb
W create super class for page plugins
W implement "creation of files only when necessary" (i.e. if dependencies have changed)
    use modification of source and target to check if source is newer -> changed
    change the behaviour in file copy plugin
W think about configureable list of files patterns which should always be ignored when reading src dir
W replace current logging mechanism with Log4r
W implement <template> meta-info tag (see template based)
W redo tree transformer
W refacture menu tag plugin
W update default.css so that the page display correctly
N convert everything to utf8 before writing -> the file need to have the correct encoding
W better loggers for plugins (substitute real plugin name for "plugin")
N think about logging levels and when to use which level
W add multiple language support
W speed up menu creation by caching the created menu and only substituting the relative paths
W if a page in one language is in the menu, all other languages for this page should also be in the menu (-> de yaml project page, no selection in menu)
W link creation for nodes should be done only in one place (now in menu and navbar tags)
W navbar should be configurable (start tag, separator, end tag)
W command line parameter for listing all available configuration options with default values
W add check so that all needed metainfo tags are in read files
W eventually put TreeUtils methods into Node
W get_node_for_string should also work for absolute paths (ie. '/images' or '/projects/index.html')
W relocatable tag fails for "{relocatable: /images/bghack.png}" in index.xpage
W make get_relpath_to_node work for all nodes, not only page nodes
W implement meta info backing file for page file types without metainfo (e.g. html fragments)
W file name syntax: [0-9].name-with_blanks.[de].[EXTENSION] -> title: Name with blanks, Order: [0-9], Language: de
W implement order tag for setting menu order
W use rake, gem and setup.rb for installing and packaging
W prevent creation of log file if nothing is done (on -p, -e, --help...)
W put main and configuration class into the Webgen module
W use code coverage tool (-rcoverage) for checking the coverage on the testSite (use with rake)
W document existing files (RDOC)
W check out http://validator.w3.org/check/referer -> use xmllint from libxml2 package
W make file plugins case-insensitiv (convert all uppercase letters to lowercase)
W generate VERSION file when packaging
W list plugins by category instead of name (file handlers and tags should be grouped together)
W make get_node_for_string work with page files as parameters (reference in home.xpage to projects page)
W make get_node_for_string work with localized page names, i.e. /index.html/index.de.html to get the german version of the page
W add option to lang tag: showAlways -> display lang menu when only one item would appear?
W handle configuration options for tags uniformly (default configuration and per tag configuration)
W add option to menu tag: showOnlyThisSubtree -> displays only one tree to a deeper level
W redo TreeWalker -> tree should be walked separately for each registered plugin
W add virtual menu nodes for external html files
W add mechanism to document configuration parameters
W modify tag configuration so that mandatory/optional arguments for tags are supported
W add RedCloth and BlueCloth and RDOC page description files
W use File.basename(filename, '.*) to strip ext, File.extname(filename) to get ext
W add debug statements to method_added in configuration.rb
W do not allow two or more page files in the same language, print warn
* remove log4r support, add STDLIB logger support
* one file format, many content formats
* allow line breaks in tags (substitute: \n -> ' ', \n\n -> \n)
* configuration should be ruby file with special syntax, to allow the specification of additional tags --> additional config file
* create tags in config file which produce informational output about filehandlers and tags (param listing,...)
* include version number in documentation
* get_relpath_to_node: if destNode['external'] --> check for URL pattern
* add mandatory param info to output of 'describe'
* option for includeFile tag: escapeHTML (should special html tags be escaped?)
* more tags (in docu: how to use them, parameters, ..., structure should be the same everywhere)
* add facility to check output files on conformity to a HTML standard
* tag 'wikilink'
* file handler for 'picture gallery' files
* add optional extension method to Plugin class
* rename tag loader -> extension loader
* document extension loader and its config file
* document in file handler that :dir extension is used for directory handler
* options for PageHandler: default file format if none given
* state that if a dependency is not found (e.g. redcloth) the plugin is disabled and a warning issued
* add example section for each plugin
* add tests for all parameters with which webgen can be called
* Bug in analyse_filename if langpart.length != 2 (write test!) -> was already handled gracefully ;-)
* remove Plugin.plugin method -> use class name! (short class name, without modules, Test::Hallo.name.split(/::/).last )
* remove extension(...) call from file handlers -> DefaultHandler#initialize
* pagehandler + converter into own subdir
* think about filename conflicts for picture pages
* document that webgen documentation source files (as example website) are in the tar.gz file!
* document philosophy of webgen (never stop running, errors/warnings, default values,...)
* extend plugins section on main documentation page
* add design gallery
* register with Freshmeat.net
* check out http://jigsaw.w3.org/css-validator/check/referer -> using xmllint
* BUG DirHandler#recursive_create_path: handle relativ paths ('..' and '.')
* allow specification of metadata for gallery and picture pages
* create thumbnails of pictures on the fly with rmagick
* do not create thumbnails if they already exist and the source file has not changed
* ev. create a GalleryNode and put gallery/pictures pages beneath it??? -> No
* tag 'sitemap' -> generate site map
* add pages for content converter and layouter plugins
* pictureGallery: document layout methods (how to extend -> write subclass, can use config values as layouters are plugins), main things...
* pictureGallery: files param: no path outside src directory
* simplify DefaultContentHandler, DefaultHTMLValidator, DefaultGalleryLayouter, ...
* Webgen::Plugin.config should have classes (not class names) as keys
* integrate check on output HTML pages (using xmllint)
* change extension.config to output registered handlers
* add htmlvalidator to docu
* document TreeWalker plugin -> not needed
* restructure documentation (filehandler, contenthandler, htmlvalidator, tags)
* tag 'mrplot' -> generate a plot with mrplot --> tag not very useable
* ordering of picture/gallery files for menu
* add params to menu for style classes (webgen-submenu, webgen-menuitem-selected) -> see menu feedback on wiki
* add parameters for controlling log files
* implement feature request #1642
* create webgen 0.2.0 to 0.3.0 converter -> No
* Bug in wikilink: & not supported
* have a look at wiki -> done
* remove PageNode nodes from tree and put ***.page nodes directly in dir??? (see directory/backing/page handler)
* refactor backing file structure: reference files directly, not via basename/language
* site specific plugins in plugin directory
* add a getting started section
* describe new CLI on extra page (documentation/cli.page)
* log files goto log directory
* create file for logging plugin
* document the existence of the mailing list
* webgen create -> create basic template/css + index.page
* update FileCopyHandler
* menu order: if order number equal, sort alphabetically
* RF bug: sitemap displays multilingual sites more times, sitemap should respect menuOrder
* reloctable tag: correct output for complex URLS???
* document the various meta info entries that the plugins use (inMenu, menuOrdering, ...) --> define: Plugin.used_meta_info( name, desc )
* create logo "Created with webgen"
* implement command "webgen clean" (options -f (force) -a (all files in dir, not only generated ones)
* BUG: relocatable should work if defaultLangInFilename is true (eg. copy.html -> not found because of copy.en.html)
* refactor menu: abstract menu generator+parser, concrete impls (vert. menu, horz. menu, ...)
* change langbar like in mail from Thomas Werschleinvn
* mention which extension the file handlers work on (backing file handler, template file handler, ...)
* add doku for new menu builder plugins, adjust menu.page
* test Node#relpath_to_string
* add possibility to add user-defined resources via config.yaml
* MAYBE: ONE page node for all pages, metainfo 'en' => all info about english, 'de' => same, ...??? --> better not
* add style advices for menu styles
* resources should be saved in Webgen::Plugin.config
* make tag plugin for predefinedResources work
* document ResourceManager
* specify on menu style pages in which browsers the menus work
* document super classes of plugins because plugins can use parameters of super classes
* create partialMenu style which only shows a partial menu
* security concerns (setting different $SAFE level???) --> No
* create vertical-dropdown menu
* better docu for backing file handler
* make dropdown menus "look" nice in IE
* make dropdown menus work in IE -> rather hard to do in a general way
* convert smiley characters to smileys
* add keywords to default.template
* document which meta info can be used in gallery file for gallery itself, gallery pages and picture pages
* summary page of all parameters
* add note that smellies emoticon pack is from ... -> see temp/smileys dir
* docu for webgen-gallery-editor
* docu on how to extend webgen
* custom commands provided by plugins
* tag 'download' which links to a download file and appends its size and ev. an image (PDF, ...)
* modify tags system sothat tags can be specified via a tag directive (like extension with file handlers)
* adapt clidesc to cmdparse 2.0.0
* webgen show config => output same info as on docu pages
* test usage for en, deu, eng (ISO-639-2 codes)
* rename Plugin.add_param to Plugin.param
* rename ContentHandlers to ContentFormatters
* how to use Node#resolve_node to resolve page nodes given like index.page (tree uses output names)???
* think about plugin system, @@config etc - should work from within webgen-gui, not only once from webgen
* FileCopyHandler#initialize not reentrant
* allow a template to use another template (templates as page description files?) (template2 uses template1)
* refactor code sothat one source file can be used by 0..n processors
* implement plugin which creates nodes for fragment identifiers (#info) in html
* Node#resolve_node: query part has to be removed from URL for this function to work correctly -> no, works with query part too!
* ruby 1.8.2 (2004-11-06) [i386-mswin32]: bug in File.join: File.join('asdf', '/') -> 'asdf//' instead of 'asdf/' -> works with 1.8.4
* BUG make tc_tags_executecommand work on windows (with windows echo command)
* test on Windows with Ruby 1.8.4
* make tests independent from RedCloth (overwrite default content converter for PageFileHandler)
* rename tags/tags.rb to tags/tag_processor.rb
* is metainfo orderInfo incorrectly named? sortInfo better? -> Node#order_info --> let this be orderInfo for now
* test on windows after update to newest ruby version!
* add test case for SmileyReplacer
* implement system which allows to specify which files/types of files should be read in which order by FileHandler
* renamed handle_extension, handle_path_pattern to register_extension and register_path_pattern
* new semantics for param changehandler: if set, use it to set the new value (useful for updating hashs instead of overwriting) --> no changehandler anymore
* ev. use change handler for not substituting PageHandler:defaultPageMetaData, but merging with old values --> no changehandler anymore
* add object for main gallery page to galleryinfo object
* depends_on accepts only strings
* implement output backing file functionality
* mail to comic site creator that nav links for gallery in main template now possible
* Catch ERB errors
* all nodes should be created through a FileHandler method (should be used by, e.g., GalleryFileHandler)
  - method should have hooks before_create_node and after_create_node
* plugin#param calls are expensive, use local variable if possible (eg. TemplateFileHandler#get_default_template)
* use relocatable tag in gallery templates for page and image references
* create tag block for substituting page blocks
* add tests in page files to check if used language code is correct (use LanguageManager)
* provide more hooks into system
* add syntax highlighting to includefile tag, executecommand tag, and ev. others
* relocatable: path has to be changed so that it has correct lang in it if it is a page node path or split in path + fragment and search for path, then get correct lang node, then search fragment
* gallery layouts not with plugins, but with nested templates (one for each: main page, gallery pages, pictures page)
  put a special GalleryInfo object in node.node_info[:ginfo] which holds all information about the gallery and
  provides helper functions (next_pindex, next_gindex,...)
  -> provide a command to copy provided gallery layout templates to a website dir (webgen use gallerylayout <name>)
  -> "webgen use" can also be used for: webgen use style
  -> then its possible to put navigational buttons outside of the {block: content}, namely in the super template
     (see mail from comic site author)
* new option for create command: -u --update-style-only (for only updating the style info) -> see above webgen use style
* problem loading a website twice due to using require -> now loading site specific plugins into a module
* use new scheme for plugin names which does not use the class name
* add a few good opensource website styles (e.g. some from andreas viklund)
* add CLI command plugins again
* use Text::Format or something similar for formatting CLI output (e.g. webgen help create) -> using own simple formatter method
* css styles should NOT do styling of content!!! only whats necessary to fulfill the job (eg. dropdown behaviour)
* add parameter to vertical menu style: startLevel -> level at which menu should be started -> use it in andreas06/plain website style
* print files copied by create and use commands!
* andreas01 style bug: when no submenus, menu is moved down a little bit -> mail to a.viklund
* partial menu: should only show menu level UNDER the current level (i.e. if a dir on top level selected, show pages in dir) -> use vertical style with startLevel dynamically set via ERB
* add possibility to only update one file -> instead of whole site, one or more files can now be generated
* eventually use two levels of info: one for infos that should be displayed normally and one for additional infos
  - always displayed: node creation, node writing, thumbnail writing
  --> not two info levels: the messages that are too verbose go into the debug level
* add posibility to add attr=value pairs to links (in the #link_from method, use a meta_info prop link_attrs)
* redo docu for webgen run command: Runs webgen, ie. generates the HTML files
* BUG command clean does not delete all files (e.g. no resources, other files generated by plugins [source plugin]) -> no clean command anymore, maybe later again when caching of website nodes is possible
* make webgen source installation work with correct cmdparse version -> installation via rubygem is the preferred way, and much easier
* new configuration file syntax:
  - should be ruby file -> still yaml for consistency (meta info in page file and backing file are yaml)
  - should allow changing of parameters with opt. updater function -> not implemented
  - should allow changing of file handler patterns -> not really useful (would be like changing the ext. of a OO.o
* better system for setting default page meta data like default content format/block names etc -> see above
* integrate xml builder for HTML/XML generation
* file handler for rcss files (CSS files with ERB code) -> redo website style CSS files in rcss
  not only rcss but any kind of preprocessed files, include param paths like copy handler,
  strip the letter r from extension: default.rcss -> default.css, mytest.rtxt -> mytest.txt
  -> handle this via copy handler, new param erbPaths -> are preprocessed with erb
* remove SYNTAX_HIGHLIGHTING constant by using a method in manager which returns if an optional part is loaded
* new CLI command for checking availability of add. libraries (opt. libraries should be wrapped by a special method load_optional_part() )
  parameters: :name, :error_msg, :about, :needed_gems
* new CLI command for checking configuration file
* include exif info for pictures (use exif library from RAA) -> using exifr gem as it is available as gem and pure ruby!
* pre-compile ERB templates -> should give a little speed boost -> which it does a little ;-)
* Node#route_to can't handle '..'!!! -> now it can by using the pathname library
* implement static menus, ie. if a file menu.yaml is in the website dir, use it instead of building menu trees
  - file format simliar to page files, one block for a menu tree in one language, if menu trees for more than one
    language then use more blocks with e.g. "--- de" separators, if no separator at beginning of file -> menu tree
    for default language
  - need to rethink the definition of a menu tree for static menus where categories do not need to be directories
  -->> implemented, but differently: not menu.yaml, but 'static' menus are defined using the output backing mechanism
* parameter for setting the orderInfo of index pages so that they always appear as first entry
  -> no really useful, one can just set the orderInfo using the meta info backing file or in the index files themselves
* optimize param_for_plugin by not using exception but return values (return [param_found, param_value])
* optimize PluginManager#plugin_class_for_name (store plugin_classes as hash)
* it seems that precalculating the param values and then using them does not boost the speed so much :-(
* maybe: precalculate full_path, absolute_path, to_url when parent or path changed -> huge speed boost!!!
* make website generation faster (webgen 0.3.8: ~6.5s, new webgen ~13.0s!)
* adapt GalleryInfo object: add a general data object which holds general information about the gallery (imagesPerPage, thumbnailSize,...)
  -> the general data object should be unique among all gallery info objects
* document which plugin uses which resource (eg. menu plugins depend on webgen-css resource)
* new plugin naming convention:
  - Core/ - core plugins, File/ - file handler plugins, Tags/ - tag processors, Misc/ - misc plugins
  - HtmlValidator/ - html validators, MenuStyle/ - menu styles, ContentConverter/ - content converters
* document how plugin naming works (namespace/pluginname)
* documentation: create an example section where gallery styles, website style, aso are showcased!
* place a note about the spelling of webgen (all lowercase) somewhere very prominently
* Coderay version 0.7.4.215 required at least
* document how to build a menu with arbitrary section and menu items (also menu items under several different headings)
* document default plugin name (derived from class name)
* provide a sample for each website style on the webgen homepage
* describe how path patterns work -> ri Dir.glob
* file handlers which want to create a node that does already exist have to return the node
* filehandler ignores hidden directories, ie. ones with a leading dot
* depends_on only accepts strings
* document new dependencies like builder and coderay and exifr
* document on gallery style slides that collage/slide images are only created if they do not exist!
* doucment that when using the slides gallery style there has to be an 'layouter: slides' entry in the gallery file!
* documentation: put api specific documentation into RDOC, user specific docus on homepage
* document install page: two sections (Download/Install) and refer to special windows install issues
* document fields for plugin infos method
* docoument new CLI commands (check config/libs, use gallery_style/website_style, ...)
* loader.load_from_dir/load_from_file wraps the loaded classes in a module, load_from_block does not!
* document that website styles have be edited and full demos are available on the given homepage
* documentation: what else to say generally about file handlers?
* documentation: plugin names should have the form namespace/namespace/.../name where namespace and name should only have alphanumeric chars
* show available styles and templates in docu and in online help (webgen help create)
* documentation: new meta info linkAttrs used in links: should be a hash of attribute-value pairs which will be added to a link to this page node
* document linkAttrs meta info (eg. used for adding custom attributes to links, e.g. custom icons using css or accessibility features, use backing file for setting icons for directories)
* the use commands only copy files, they do not delete files in the src dir
* document that source directory is now always called src - no parameter anymore
* document somewhere which objects are available when processing templates/pages files with ERB
* document evaluation order of page files (first content converted from Textile/Markdown/... to HTML, then ERB, then tags)
* create a rake task for running webgen (like the rdoc task)
* documentation: split getting started into basics and tutorial pages
* make it possible to set meta information template to nil
* check styles for correct selected menu items (due to new <span> element, e.g. andreas07)
* change namespace of tag plugins from tags to tag
* fix website styles menus to have a style for current menu item with <span> instead of <a>
