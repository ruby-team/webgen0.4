#
#--
#
# $Id: html.rb 531 2006-11-17 17:56:14Z thomas $
#
# webgen: template based static website generator
# Copyright (C) 2004 Thomas Leitner
#
# This program is free software; you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program; if not,
# write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
#
#++
#

load_plugin 'webgen/plugins/contentconverters/default'

module ContentConverters

  # Handles HTML content. Assumes that the content is already valid HTML.
  class HtmlConverter < DefaultContentConverter

    infos( :name => 'ContentConverter/HTML',
           :author => Webgen::AUTHOR,
           :summary => "Handles HTML formatted content"
           )

    register_handler 'html'

    def call( content )
      content
    end

  end

end
