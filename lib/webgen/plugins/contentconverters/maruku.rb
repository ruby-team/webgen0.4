load_optional_part( 'content-converter-maruku',
                    :needed_gems => ['maruku'],
                    :error_msg => "Markdown not available as content format as Maruku could not be loaded",
                    :info => "Markdown + (extra) can be used as content format" ) do

  require 'maruku'
  load_plugin 'webgen/plugins/contentconverters/default'

  module ContentConverters

    # Converts text formatted in Markdown format using Maruku to HTML.
    class MarukuConverter < DefaultContentConverter

      infos( :name => 'ContentConverter/Maruku',
             :author => "Andrea Censi",
             :summary => "Handles content formatted in Markdown format using Maruku "+
                          "http://maruku.rubyforge.org/"
             )

      register_handler 'maruku'

      def call( content )
        Maruku.new( content ).to_html
      rescue Exception => e
        log(:error) { "Error converting Markdown text to HTML: #{e.message}" }
        content
      end

    end

  end

end
