#
#--
#
# $Id: markdown.rb 531 2006-11-17 17:56:14Z thomas $
#
# webgen: template based static website generator
# Copyright (C) 2004 Thomas Leitner
#
# This program is free software; you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program; if not,
# write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
#
#++
#

load_optional_part( 'content-converter-haml-sass',
                    :needed_gems => ['haml'],
                    :error_msg => "Haml and sass not available as content format as the haml library could not be loaded",
                    :info => "Haml/Sass can be used as content format" ) do

  require 'haml'
  load_plugin 'webgen/plugins/contentconverters/default'

  module ContentConverters

    class HamlConverter < DefaultContentConverter

      infos( :name => 'ContentConverter/Haml',
             :author => Webgen::AUTHOR,
             :summary => "Handles content formatted in Haml format"
             )

      register_handler 'haml'

      def call( content )
        Haml::Engine.new( content ).render
      rescue Exception => e
        log(:error) { "Error converting Haml text to HTML: #{e.message}" }
        content
      end

    end

    class SassConverter < DefaultContentConverter

      infos( :name => 'ContentConverter/Sass',
             :author => Webgen::AUTHOR,
             :summary => "Handles content formatted in Sass format"
             )

      register_handler 'sass'

      def call( content )
        Sass::Engine.new( content ).render
      rescue Exception => e
        log(:error) { "Error converting Sass text to HTML: #{e.message}" }
        content
      end

    end

  end

end
