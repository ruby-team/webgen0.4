load_plugin 'webgen/plugins/tags/tag_processor'

module Tags

  class HtmlMetaInfo < DefaultTag

    infos( :name => 'Tag/HtmlMetaInfo',
           :author => 'Andrea Censi',
           :summary => 'Writes the content of the "description","author","keywords" as HTML META tags.')

    register_tag 'htmlmetainfo'

    def process_tag( tag, chain )
      cur_node = chain.last
      s = ""
      ['description', 'author', 'keywords'].each do |key|
         if value = cur_node.meta_info[key]
            value = CGI::escapeHTML(value)
            s += "<meta name='#{key}' content='#{value}'/>\n"
         end
      end
      s
    end

  end

end
