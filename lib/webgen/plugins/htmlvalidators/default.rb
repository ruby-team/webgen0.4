#
#--
#
# $Id: default.rb 531 2006-11-17 17:56:14Z thomas $
#
# webgen: template based static website generator
# Copyright (C) 2004 Thomas Leitner
#
# This program is free software; you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program; if not,
# write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
#
#++
#

module HtmlValidators

  class DefaultHtmlValidator < Webgen::HandlerPlugin

    infos( :name => 'HtmlValidator/Default',
           :author => Webgen::AUTHOR,
           :summary => "Base class for all HTML validators"
           )

    # Validates the file +filename+. Has to be overridden in subclasses!
    def validate_file( filename )
      raise NotImplementedError
    end

  end

end
