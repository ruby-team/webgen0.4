#
#--
#
# $Id: horizontal.rb 531 2006-11-17 17:56:14Z thomas $
#
# webgen: template based static website generator
# Copyright (C) 2004 Thomas Leitner
#
# This program is free software; you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program; if not,
# write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
#
#++
#

load_plugin 'webgen/plugins/menustyles/default'

module MenuStyles

  class HorizontalMenuStyle < DefaultMenuStyle

    infos( :name => 'MenuStyle/Horizontal',
           :author => Webgen::AUTHOR,
           :summary => "Builds a horizontal menu"
           )

    param 'startLevel', 1, 'The level at which the menu starts. For example, if set to 2 the top most ' +
      'menu items are not shown.'
    param 'maxLevels', 3,  'Specifies the maximum number of levels that should be shown. ' +
      'For example, if maxLevels = 1, then only one level is shown.'

    register_handler 'horizontal'

    def internal_build_menu( src_node, menu_tree )
      unless defined?( @css_added )
        @plugin_manager['Core/ResourceManager'].append_data( 'webgen-css', "
/* START webgen horizontal menu style */
.webgen-menu-horiz ul { display: block; }
.webgen-menu-horiz li { display: inline; }
/* STOP webgen horizontal menu style */
" )
        @css_added = true
      end
      "<div class=\"webgen-menu-horiz #{param('divClass')}\">#{submenu( src_node, menu_tree, 1 )}</div>"
    end

    #######
    private
    #######

    def submenu( src_node, menu_node, level )
      if menu_node.nil? \
        || level > param( 'maxLevels' ) + param( 'startLevel' ) - 1 \
        || src_node.level < param( 'startLevel' ) \
        || !src_node.in_subtree_of?( menu_node.node_info[:node] )
        return ''
      end

      submenu = ''
      out = "<ul>"
      menu_node.each do |child|
        submenu << (child.has_children? ? submenu( src_node, child, level + 1 ) : '')
        style, link = menu_item_details( src_node, child.node_info[:node] )
        out << "<li#{style.to_s.empty? ? '' : ' ' + style}>#{link}</li>"
      end
      out << "</ul>"
      out << submenu

      if level < param( 'startLevel' )
        '' + submenu
      else
        out
      end
    end

  end

end
