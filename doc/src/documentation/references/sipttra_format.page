---
title: Sipttra Format
inMenu: true
---

h1. The Sipttra Format


h2(#sipttra). What is sipptra?


sipttra is the acronym for <b>S</b>imple <b>P</b>lain <b>T</b>ext <b>T</b>racker. It is a ticket
tracker which stores everything inside a human readable text file. Therefore it can be used with any
text editor and no additional program to edit it is needed.

sipttra supports categories with different types, a special milestone category and, naturally,
tickets.

As everything is stored in a text file, sipttra is not intended to be used by many concurrent users.
It is a simple and easy system for one-man-projects as well as small projects with not many
developers.


h2(#structure). Types


sipttra is line and not character based. This means that it only operates on whole lines and each
line can have a different type. This section describes the available types and the next section
explains how these types can be used in a sipttra file.


h3(#categories). Categories


A category has a name and a type which have to be unique among all categories. Therefore it is not
possible to define two equal category lines in one sipttra file. Tickets can only be defined for
categories. The category type further distinquishes the tickets of a category name. For example,
normally you have at least open bugs and closed bugs. For sipttra, this means that you have defined
a category named bugs with type open and another category named bugs with type closed.

A category is defined like this:

<pre>
  ### CATEGORY (type) ###
</pre>

or

<pre>
  # CATEGORY (type) #
</pre>

The line has to start with one or more hashes, the category name, the category type and finally the
same number of hashes as at the start of the line. These items have to be separated from each other
by spaces.

The tickets of a category are of the same type as the category itself. The special type @closed@
signifies that the tickets in the category are closed, tickets from all other category types are
considered to be open.


h3(#milestones). Milestone Category


The milestone category is defined like a normal category. However, the type in parenthesis is
omitted, like this:

<pre>
  ### Milestones ###
</pre>

A sipttra file can contain exactly one such milestone category! This is the only distinction between
normal categories and the milestone category.


h3(#tickets). Tickets


Tickets are defined like this:

<pre>
   * NAME (YYYY-MM-DD) [BELONGS_TO] summary
     further description

     still in the description
</pre>

Basically all parts are optional. However, if no summary is specified it is assumed to be empty. The
name uniquely distinguishes the ticket among all tickets independent of the category. The date in
parenthesis is considered to be the due date, ie. the date on which this ticket should be closed.
The 'belongs to' part in square brackets defines a relationship to another ticket. The summary part
is a short description of the ticket. All following lines which are either empty or indented exactly
two spaces belong to the detailed description of the ticket (one can use Markdown syntax in the
detailed description). *Caveats*: if one wants to only specify a name and a summary for a ticket,
these two parts have to be separated by a colon sothat the parser can distinguish them. Also be
aware that the spaces between the individual parts are mandatory!

Here are some examples for valid tickets:

<pre>
  * name1: summary

  * name2 (2007-02-15) my summary here

  * name3 [othername] yeah, this also works
    with additional description

    which still belongs to ticket name3
</pre>

Two important attributes for tickets are defined indirectly:

* status: The status is defined by the category type to which a ticket belongs
* priority: The priorites of the tickets are defined by their order. The ticket defined first in a
  category has the highest priority, the last defined ticket the lowest.

Tickets for the milestone category are defined like normal tickets. However, milestone tickets must
have a name defined!


h3(#comments). Comments


Comment lines are allowed everywhere in a sipttra file! Each line which is neither a category line,
a ticket line or a line belonging to an additional description of a ticket becomes a comment line.
Also be aware that if a line for a ticket or category is not well structured, it automatically
becomes a comment line.


h2(#fileformat). The sipttra File Format


A sipttra file starts with an optional meta information part. If you want to have a meta information
part the sipttra file has to start with three dashes on the first line. Everything till the next
line with three dashes is considered to be meta information. This meta information part has to be
valid YAML.

The main part consists of the tracker definition. It is read line by line from top to bottom:

* All lines before the first category line are considered to be comment lines.
* Tickets always belong to the last defined category.
* The same category can not be defined twice in one sipttra file!


h2(#examples). Examples


Here are some example sipttra files.

<pre>
\---
webgen-metainfo:
  inMenu: true
  title: Project Status
\---

### Milestones ###

* Feb07 (2007-02-28) Bug fixes and small enhancements
  - include patches/requests created since 0.4.1 release
  - add support for sipttra files, the Simple Plain Text Tracker

* F: Ideas and todo items for future versions

  This milestone holds all ideas/todo items/requests which are implemented some
  time in the future.


Here could be some potential comment! :)

### Features (open) ###

* T001 [Feb07] one ticket

* T002 [F] another ticket
  this is additional text


### Features (closed) ###

* T003 [F] and another closed ticket


### Bugs (open) ###

* T008 [Feb07] you should really close this bug!


### Bugs (closed) ###

* T009 [Feb07] yeah, very cool, a closed bug!
</pre>


<pre>
This are just some
comment lines
here!

### Milestones ###

* Feb07 (2007-02-28) Bug fixes and small enhancements

Here could be some potential comment! :)

### Features (open) ###

* T001 [Feb07] one ticket

* T002 [Feb07] another ticket
  this is additional text


### Features (closed) ###

* T003 [Feb07] and another closed ticket

### Bugs (open) ###

* T008 [Feb07] you should really close this bug!


### Bugs (closed) ###

* T009 [Feb07] yeah, very cool, a closed bug!
</pre>


<pre>
### Features (open) ###

* T001 [Feb07] one ticket

* T002 [F] another ticket
  this is additional text


### Features (closed) ###

* T003 [F] and another closed ticket
</pre>
