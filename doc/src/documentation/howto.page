---
title: Howto...?
inMenu: true

howtos:
  - - create a website?
    - |
      You can create the needed structure by hand (which would be not so much to do). However, it's
      still easier using the @webgen@ command line tool:

        webgen create -t project -s andreas07 my_site

      This will create a webgen website in the directory @my_site@ using the specified template and
      style.

  - - use a website/gallery/sipttra style?
    - |
      Just use the @webgen@ command line tool:

        webgen use website_style STYLE_NAME
        webgen use gallery_style STYLE_NAME
        webgen use sipttra_style STYLE_NAME

      Be aware that destination files may be overwritten!

  - - set plugin parameters?
    - |
      You can set plugin parameters for all plugins via the configuration file. For example, say you
      want to set the {param: File/DefaultHandler:linkToCurrentPage} parameter to @true@, then you
      would add the following to the configuration file:

        File/DefaultHandler:
          linkToCurrentPage: true

      There is a second possibility for tags: you can set the parameters directly in the tag
      definition, like this:

        \{breadcrumbTrail: {omitLast: true, separator: ' | '}}

  - - change the default language?
    - |
      To use, for example, German as the default language, put the following into the configuration file:

        Core/Configuration:
          lang: de

  - - set a different content format for page files?
    - |
      If you want to change the default content format, you need to add the following to your
      configuration file (assuming you didn't set the {param: Core/FileHandler:defaultMetaInfo} parameter):

        File/PageHandler:
          defaultMetaInfo:
            blocks: [[content, FORMAT_NAME]]

      This will set the default format to FORMAT_NAME.

      If you just want to change the format for a block, you should do it like this:

      <notextile><pre><code>--- content, FORMAT_NAME
        This is the block
      </code></pre></notextile>

  - - set the default meta information for files created by a specific file handler?
    - |
      Use the configuration file! For example, to change the default content format
      to markdown and @inMenu@ to @true@ for page files use the following in your configuration file:

        File/PageHandler:
          defaultMetaInfo:
            blocks: [[content, markdown]]
            inMenu: true

  - - ignore files in the source directory?
    - |
      This can be done using the {param: Core/FileHandler:ignorePaths} parameter. For example, to
      ignore all files starting in @core@, you would put the following in the configuration file:

        Core/FileHandler:
          ignorePaths: [**/core*]

      Be aware that the parameter overwrites the default setting. For more information on which
      path patterns are supported, have a look a the
      <a href="{relocatable: plugins/core/filehandler.page#pathpattern}">path pattern documentation</a>!

  - - change the output name format of page file?
    - |
      Set the {param: File/PageHandler:outputNameStyle} in the configuration file to globally change
      how the output names for page files are constructed or use the @outputNameStyle@ meta
      information to change it only for individual page files!

      For more information have a look at the documentation of the
      {param: File/PageHandler:outputNameStyle} parameter!

  - - use embedded ruby in page/template files?
    - |
      Have a look at the <a href="{relocatable: references/webpage_format.page#proc-erb}">ERB section</a> of
      the WebPage Format reference!

  - - modify the template chain?
    - |
      First, have a look at the {plugin: File/TemplateHandler} documentation!

      To stop the template chain at a specific template or even at the page file itself, specify a
      null template in the meta information, like this:

        template: ~

      To nest templates, you just need to specify the template in the meta information section:

        template: my_special.template

      Be aware that if no @template@ meta information is specified for a page or template file, the
      template handler automatically searches for a default template in directory and the parent
      directories of the file!

  - - localize a directory name?
    - |
      Just set the @directoryName@ meta information on the localized directory index files!

  - - provide additional attributes on links to a page file?
    - |
      You can specify additional attributes for a page file using the @linkAttrs@ meta information.
      Take the following page file:

      <notextile><pre><code>---
      title: Tutorial
      inMenu: true
      linkAttrs:
        title: This is a rather large tutorial
        accesskey: D
        tabindex: 5
      ---
      Yippieh, this is my tutorial!
      </code></pre></notextile>

      When a link to this page is created the specified attributes always get set on this link!

  - - add page specific sidebar content?
    - |
      There are many ways to accomplish this, I will show only one way here using blocks. Add the
      following to the sidebar in your @default.template@:

        <% if node.node_info[:pagedata].blocks.has_key?( 'sidebar' ) %>
          \{block: sidebar}
        <% end %>

      This will include the contents of the block @sidebar@ in the sidebar if such a block exists
      for a page. Then just add a sidebar block to each page file which needs it. Following is such
      a sample page file:

      <notextile><pre><code>This is the main content block
      --- sidebar
      This is the sidebar block and everything in here goes to the sidebar!
      </code></pre></notextile>

  - - use global variables?
    - |
      First you need to set some global variables in your configuration file:

        Core/Configuration:
          customVars:
            myVar: This is really nice!
            myOtherVar: another content

      Then you can use the {plugin: Tag/CustomVar} to output the variables:

        \{customVar: myVar}

  - - create XML output?
    - |
      This can be achieved manually (by setting the content format of the page file to @html@ and
      then directly creating the XML elements) or by using the {plugin: ContentConverter/XmlBuilder}
      plugin !

  - - use emoticons instead of ASCII smileys?
    - |
      Either globally enable an emoticon pack in the configuration file, like this:

        Misc/SmileyReplacer:
          emoticonPack: crystal

      Or enable smiley replacement only on a specific page file using the meta information, like
      this:

        emoticonPack: crystal

  - - syntax highlight an included file?
    - |
      This can be done using the {plugin: Tag/IncludeFile} plugin. Make sure you have @coderay@
      installed! Here is an example which includes a syntax highlighted ruby file:

        \{includeFile: {filename: happy.rb, highlight: ruby}}

  - - create a static menu?
    - |
      Have a look at the <a href="{relocatable: plugins/core/filehandler.page#metainfo-file}">meta information backing file</a> documentation!

      You can use virtual nodes in the output backing section to define virtually any menu structure
      you like, including things like having menu entries that point to the same page and links to
      external pages!

---
h1. How to...?


This is a collection of some commonly asked questions on how to accomplish certain things with
webgen. If you have an idea for a new howto, don't hestitate to write me a
<a href="mailto:t_leitner@gmx.at">email</a>!

<notextile>
<ul>
<% node['howtos'].each_with_index do |howto, h_index| %>
  <li><a href="javascript: toogle('howto-<%= h_index %>')"><%= howto[0] %></a>
      <div id="howto-<%= h_index %>" style="display: none">
        <%= @plugin_manager['ContentConverter/Textile'].call( howto[1].to_s ) %>
      </div>
  </li>
<% end %>
</ul>
</notextile>
