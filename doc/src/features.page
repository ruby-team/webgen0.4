---
title: Features
inMenu: true
---
h2(#features). Feature list

* <h3>webgen core</h3>

  * Easily *extendable* through plugins
  * Easy to install and use
  * Easy to configure if one needs to (no need if you are happy with the default values)
  * Command line interface uses command style syntax (like Subversion's @svn@ command)
  * *Fast*

* <h3>File Handler (for handling the files in the source directory)</h3>

  * Powerful file copy handler for copying single files or entire directories to the output directory
  * Support for *image galleries* -> generates entire image galleries, with *automatic thumbnail creation*
  * ... and much more (see {plugin: File})!

* <h3>Page Files (for defining web pages)</h3>

  * Support for *ERB* (embedded Ruby) in page files and in templates
  * Support for *nested templates*
  * Supports several *different content formats* (Textile, Markdown, RDOC, xml builder, plain HTML), new ones are easy to add
  * Support for specifying meta information (e.g. title, menu ordering information, ...)
  * ... also see {plugin: File/PageHandler}!

* <h3>Tags (used for easily adding dynamic content to web pages)</h3>

  * Standard distribution provides often used tags
  * *Menu tag* can generate different types of menus (simple menus and ones with CSS drop downs)
  * *Breadcrumb trail tag* generates a breadcrumb trail so that one always knows where he is in the hierarchy
  * Relocatable tag for automatically generating correct relative paths to files
  * ... and much more (see {plugin: Tag})!

* <h3>Other Features</h3>

  * Automatically checks generated files if they are standard conform (see {plugin: HtmlValidator})
  * Supports 'virtual files/directories' via meta information backing files
  * Support for *automatic smiley replacement* with emoticons(:-) - see {plugin: Misc/SmileyReplacer})
