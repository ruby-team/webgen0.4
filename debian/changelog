webgen0.4 (0.4.7-8) UNRELEASED; urgency=medium

  [ Cédric Boutillier ]
  * Bump debhelper compatibility level to 9
  * Remove version in the gem2deb build-dependency
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Tue, 13 Aug 2019 08:27:59 +0530

webgen0.4 (0.4.7-7) unstable; urgency=low

  * Conforms to standards 3.9.3
  * Force building witg/using ruby1.8, there are subtle things that escape
    me with 1.9.1 (closes: #676208)
  * Force building with recent gem2deb
  * Refreshed all patches

 -- Vincent Fourmond <fourmond@debian.org>  Sun, 24 Jun 2012 00:53:10 +0200

webgen0.4 (0.4.7-6) unstable; urgency=low

  * Convert to gem2deb
  * Now conforms to standards 3.9.2

 -- Vincent Fourmond <fourmond@debian.org>  Thu, 02 Feb 2012 15:26:17 +0100

webgen0.4 (0.4.7-5) unstable; urgency=low

  * Improved naming of patches for diplomatic reasons... 
  * Rename all webgen directories as webgen0.4, to not install the same
    files as other webgen versions. Now, really closes: #613126.
     -> webgen-to-webgen0.4.diff

 -- Vincent Fourmond <fourmond@debian.org>  Sun, 13 Feb 2011 23:43:14 +0100

webgen0.4 (0.4.7-4) unstable; urgency=low

  [ Paul van Tilburg ]
  * debian/control:
    - Added myself to the uploaders.
    - Bumped standards version to 3.8.4.
    - Fixed the build-depend on webgen to be << 0.4 instead of < 0.4.
  * debian/watch:
    - Adapted the watch file to only consider 0.4.x releases.

  [ Vincent Fourmond ]
  * Switched to format 3.0 (quilt)
  * Dropped all references to quilt
  * Conforms to standards 3.9.1
  * Install the executable as webgen0.4
  * Added versioned conflicts to other packages installing the webgen
    binary.
  * Setup alternatives for providing the webgen binary and the
    corresponding manual page (closes: #613126).

 -- Vincent Fourmond <fourmond@debian.org>  Sun, 13 Feb 2011 02:06:59 +0100

webgen0.4 (0.4.7-3) unstable; urgency=low

  * Added myself and the Debian Ruby team to uploaders
  * textile-line-breaks.diff: Fix the behaviour of the Textile plugin with
    respect to line breaks (closes: #525553)
  * Added a debian/README.source
  * Now conforms to Standards 3.8.3
  * In addition, it seems that the FTBS on amd64 does not occur anymore,
    though the reason why isn't very clear (closes: 533957)
  * Bumped dh compatibility level to 5
  * Added ${misc:Depends} for potential dh_-induced dependencies

 -- Vincent Fourmond <fourmond@debian.org>  Tue, 13 Oct 2009 20:54:46 +0200

webgen0.4 (0.4.7-2) unstable; urgency=low

  [ Vincent Fourmond ]
  * Add Build-Conflicts on libxml2-utils because the build hangs if
    xmllint is found.
  * Add Build-Depends on graphviz for the documentation

  [ Arnaud Cornet ]
  * Fix use of DEB_RUBY_LIBDIR that changed semantics in ruby-pkg-tools 0.14.
    (Closes: #486500), build depend on ruby-pkg-tools >= 0.14.
  * Merge Vincent's patch.
  * Add build conflict with webgen < 0.4.
  * Bump standard version to 3.8.0 (no change needed).

 -- Arnaud Cornet <acornet@debian.org>  Sun, 22 Jun 2008 16:35:34 +0200

webgen0.4 (0.4.7-1) unstable; urgency=low

  * New Upstream Version
  * Bump standards version (no change needed).
  * Move -doc package to doc section.

 -- Arnaud Cornet <acornet@debian.org>  Sat, 02 Feb 2008 12:57:09 +0100

webgen0.4 (0.4.6-1) unstable; urgency=low

  * New Upstream Version
  * Add Homepage dpkg header.
  * Update my email address.
  * Add build-deps on libmaruku-ruby and librmagick-ruby, for better doc
  generation.

 -- Arnaud Cornet <acornet@debian.org>  Wed, 24 Oct 2007 22:09:19 +0200

webgen0.4 (0.4.5-1) unstable; urgency=low

  * New Upstream Version (Closes: #432332).
  * Refresh trash_statcounter.diff.
  * debian/README.Debian: document features gained by installing each
    recommended package.

 -- Arnaud Cornet <arnaud.cornet@gmail.com>  Thu, 19 Jul 2007 15:05:35 +0200

webgen0.4 (0.4.2-1) unstable; urgency=low

  * New Upstream Version
  * Handle shabang more precisely.
  * Add optional dependencies in recommends (libbluecloth-ruby and
    libbuilder-ruby).
  * Add -doc in webgen0.4 recommends.
  * Move to quilt patch system.
  * Man page now provided by upstream.

 -- Arnaud Cornet <arnaud.cornet@gmail.com>  Sat, 12 May 2007 11:55:26 +0200

webgen0.4 (0.4.1-1) experimental; urgency=low

  * New upstream release
  * Update man page.
  * Add libexif-ruby in Recommends.
  * Change source package to webgen0.4, conflict with webgen. We make two
    different packages (webgen and webgen0.4) because they are source-
    incompatible. This is to ease the transition for webgen users.
  * Update cmdparse2.diff.
  * Cleanup debian/rules.
  * Remove webgen/gui, which is not ready.
  * Generate webgen documentation in webgen0.4-doc package.

 -- Arnaud Cornet <arnaud.cornet@gmail.com>  Sun,  4 Feb 2007 17:36:13 +0100

webgen (0.3.8-2) unstable; urgency=low

  * Update standards version to 3.7.2. Use Build-Depends-Indep instead
    of Build-Depends for ruby-pkg-tools and ruby.
  * Switch to use #!/usr/bin/ruby instead of #!/usr/bin/env ruby
    (Closes: #388357).
  * Added watchfile.
  * Use ruby CDBS class.

 -- Arnaud Cornet <arnaud.cornet@gmail.com>  Sat, 23 Sep 2006 18:23:42 +0200

webgen (0.3.8-1) unstable; urgency=low

  * New upstream "bugfix" release

 -- Arnaud Cornet <arnaud.cornet@gmail.com>  Sat, 31 Dec 2005 13:40:04 +0100

webgen (0.3.7-1) unstable; urgency=low

  * New upstream release.
  * Changed to require cmdparse2.
  * Drop binaries that use to qt.
  * Added setup.rb licence to debian/copyright.
  * Add dependency on rdoc, fixes bug #344660.

 -- Arnaud Cornet <arnaud.cornet@gmail.com>  Sat, 24 Dec 2005 20:17:46 +0100

webgen (0.3.6-1) unstable; urgency=low

  * Initial Release.

 -- Arnaud Cornet <arnaud.cornet@gmail.com>  Sun, 28 Aug 2005 13:06:39 +0200

